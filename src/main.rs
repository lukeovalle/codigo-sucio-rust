use grammers_client::{Client, Config};
use grammers_client::client::updates::AuthorizationError;
use grammers_client::types::iter_buffer::InvocationError;
use grammers_client::types::{chat, message::Message, input_message::InputMessage};
use mockall::*;

fn main() {
    println!("{}", "ola");
}

mock! {
    pub Client {
        async fn connect(config: Config) -> Result<Self, AuthorizationError>;
        async fn get_me(&mut self) -> Result<chat::User, InvocationError>;
        async fn send_message(
            &mut self,
            chat: &chat::Chat,
            message: InputMessage
        ) -> Result<Message, InvocationError>;
    }
}

async fn funcion_que_usa_client(client: &mut Client) -> Result<(), failure::Error> {
    let chat = client.get_me().await?;

    client.send_message(&chat::Chat::User(chat), "hola".into()).await?;

    Ok(())
}


#[cfg(test)]
mod tests {
    use super::*;
    use mockall_double::double;

    #[double]
    use super::Client;

    #[tokio::test]
    async fn test_mock() {
        let mock = Client::connect(
            grammers_client::Config {
                session: grammers_session::Session::new(),
                api_id: 0,
                api_hash: String::from("asd"),
                params: Default::default()
            }
        ).await;
        assert!(true);
    }

    #[tokio::test]
    async fn test_send_message() {
        let mut client = Client::connect(
            grammers_client::Config {
                session: grammers_session::Session::new(),
                api_id: 0,
                api_hash: String::from("asd"),
                params: Default::default()
            }
        ).await;
        assert!(client.is_ok());
        let Ok(mut client) = client;

        assert!(funcion_que_usa_client(&mut client).await.is_ok());
    }

}

